#ifndef _ABUNDANT_VECTOR__H_
#define _ABUNDANT_VECTOR__H_

#include <stdint.h>

namespace Euler
{
    // Constant Global Definitions
    #define MAX_ABUNDANT_LIMIT  28123
    #define MIN_ABUNDANT_NUMBER 12
    #define MIN_DIV_ABUNDANT    6

    // Type Definitions
    typedef uint16_t Abundant;
    
    // MetaData Struct
    struct AbundantMetaData
    {
        /**
         * Struct Data-Members
         */
        Abundant abnNum;
        bool     bMatching;

        /**
         * Static Functions
         */
        static bool isAbundant(Abundant snNumber);
    };

    // Abundant Main Class
    class AbundantVector
    {
        public:
            /**
             * Constructor
             */
            AbundantVector::AbundantVector() : m_lastSize(0) {}

            /**
             * Virtual Destructor
             */
            virtual ~AbundantVector() {}

            /**
             * Returns the sum of all positive numbers
             * which cannot be written as the sum of two abundant numbers.
             */
            uint32_t Result();

        private:
            /**
             * Data Members
             */
            AbundantMetaData m_arrAbundants[MAX_ABUNDANT_LIMIT];
            uint16_t         m_lastSize;
    };
}

#endif  