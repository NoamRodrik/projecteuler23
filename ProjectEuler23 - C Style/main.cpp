#include "AbundantVector.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main()
{
    uint32_t eulerResult = 0; 
    Euler::AbundantVector abundantVector;

    time_t startTime = clock();
    eulerResult = abundantVector.Result();
    time_t finishTime = clock();

    printf("It took %u milliseconds.\r\n", static_cast<uint32_t>(finishTime - startTime));
    printf("Final result: %u\r\n", abundantVector.Result());
    system("pause");
    return 0;
}