#include "AbundantVector.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

namespace Euler
{
    bool AbundantMetaData::isAbundant(Abundant snNumber)
    {
        // Variable Definition
        uint16_t nCounter = 1;

        // Code Section
        for (uint16_t nIndex = 2; nIndex <= sqrt(snNumber); ++nIndex)
        {
            if (snNumber == nIndex*nIndex)
            {
                nCounter += nIndex;
            }
            else if (snNumber % nIndex == 0)
            {
                nCounter += nIndex + snNumber / nIndex;
            }
        }

        return (nCounter > snNumber);
    }

    uint32_t AbundantVector::Result()
    {
        // Code Section
        for (uint16_t nIndex = MIN_ABUNDANT_NUMBER; nIndex <= MAX_ABUNDANT_LIMIT; ++nIndex)
        {
            // If the number is abundant.
            if (!(nIndex % 2 == 1 && nIndex % 5 == 1) &&
                (nIndex % MIN_DIV_ABUNDANT == 0 || AbundantMetaData::isAbundant(nIndex)))
            {
                this->m_arrAbundants[this->m_lastSize++].abnNum = nIndex;
            }
        }

        for (uint16_t nIndex = 0; nIndex < this->m_lastSize; ++nIndex)
        {
            // Getting the current abundant.
            const Abundant& currAbundant = this->m_arrAbundants[nIndex].abnNum;

            // Inner iteration
            for (uint16_t nInnerIndex = 0; nInnerIndex < (this->m_lastSize / 2); ++nInnerIndex)
            {
                // Getting the current inner abundant.
                const Abundant& currInnerAbundant = this->m_arrAbundants[nInnerIndex].abnNum;

                // Getting the summed abundant.
                if (currInnerAbundant + currAbundant < MAX_ABUNDANT_LIMIT)
                {
                    this->m_arrAbundants[(currInnerAbundant + currAbundant)].bMatching = true;
                }
            }
        }

        // Variable Definition
        uint32_t nResult = 1;

        // Code Section
        for (uint16_t nIndex = 2; nIndex < MAX_ABUNDANT_LIMIT; ++nIndex)
        {
            if (!this->m_arrAbundants[nIndex].bMatching)
            {
                nResult += nIndex;
            }
        }

        return (nResult);
    }
}