#ifndef _DEFINITIONS__H_
#define _DEFINITIONS__H_

/***************************************************************************************/
/*                                     Includes                                        */
/***************************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

/***************************************************************************************/
/*                                Preprocessor Definitions                             */
/***************************************************************************************/
// Printing macros
#define LOG(fmt, ...)                                                              \
do                                                                                    \
{                                                                                     \
    printf("%s:" fmt"\r\n", __FUNCTION__, __VA_ARGS__);                               \
}                                                                                     \
while (false);                                                                        \


// Checking macros
#define SANITY(condition, print)													  \
do																					  \
{																					  \
	if (!(condition))																  \
	{																				  \
		LOG("Sanity failed: %s.\r\n", print);	                                      \
		abort();																	  \
	}																				  \
}																					  \
while (false);																		  \


#define POINTER_SANITY(pointer, print)											      \
do																					  \
{																					  \
	if ((pointer) == NULL)															  \
	{																				  \
		LOG("Pointer was found null: %s.\r\n", print);	                              \
		abort();																	  \
	}																				  \
}																					  \
while (false);																		  \

#endif