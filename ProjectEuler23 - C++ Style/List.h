#ifndef _LIST__H_
#define _LIST__H_

/***************************************************************************************/
/*                                     Includes                                        */
/***************************************************************************************/
#include "Definitions.h"

namespace Tools
{
    /**
    * A list node class, made entirely of templates.
    *
    * @author Noam Rodrik
    * @since  09/30/2016
    */
    template <class T>
    class ListNode
    {
        /***************************************************************************************/
        /*                             Constructors and Destructors                            */
        /***************************************************************************************/
    public:
        /**
        * The default CTOR for ListNode.
        *
        * @param pNext The next node to point to.
        * @param data  The data the listnode holds.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        ListNode(ListNode* pNext, const T& data) : m_pNext(pNext), m_data(data) {}

        /**
        * The default copy CTOR for ListNode.
        *
        * @author Noam Rodrik
        * @since  10/02/2016
        */
        ListNode(const ListNode<T>& other) { (*this) = other; }

        /**
        * The default DTOR for ListNode.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        virtual ~ListNode() {}

        /***************************************************************************************/
        /*                                      Methods                                        */
        /***************************************************************************************/
    public:
        ListNode* GetNext() const { return (this->m_pNext); }
        void	  SetNext(ListNode* newNext) { this->m_pNext = newNext; }
        const T&  GetData() const { return (this->m_data); }
        void	  SetData(const T& newData) { this->m_data = newData; }

        /**
        * Overwriting the listnode's assignment operator.
        *
        * @author Noam Rodrik
        * @since  10/02/2016
        */
        void operator=(const ListNode<T>& other)
        {
            this->m_pNext = other.m_pNext;
            this->m_pData = other.m_pData;
        }

        /***************************************************************************************/
        /*                                      Data Members                                   */
        /***************************************************************************************/
    private:
        ListNode* m_pNext;
        T         m_data;
    };

    /**
    * A list class, made entirely of templates.
    *
    * @author Noam Rodrik
    * @since  09/30/2016
    */
    template <class T>
    class List
    {
        /***************************************************************************************/
        /*                             Constructors and Destructors                            */
        /***************************************************************************************/
    public:
        /**
        * The default CTOR for List.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        List() : m_listSize(0), m_pIterator(NULL) {}

        /**
        * The copy CTOR for List.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        List(const List<T>& other)
        {
            (*this) = other;
        }

        /**
        * The default DTOR for List.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        virtual ~List()
        {
            if (this->m_toDelete)
            {
                // For each object on the list, remove.
                while (this->m_pFirst != NULL)
                {
                    this->DeleteFirst();
                }
            }
        }

        /***************************************************************************************/
        /*                                      Methods                                        */
        /***************************************************************************************/
    public:
        /**
        * Overwriting the list's assignment operator.
        *
        * @author Noam Rodrik
        * @since  10/02/2016
        */
        void operator=(const List<T>& other)
        {
            this->m_pFirst = other.m_pFirst;
            this->m_listSize = other.m_listSize;
            this->m_toDelete = false;
        }

        /**
        * Returns the front data.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        const T& GetFrontData() const
        {
            POINTER_SANITY(m_pFirst, "The first on the list is null, failed fetching data.");
            return (this->m_pFirst->GetData());
        }

        /**
        * Adds a new item to the end of the list.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void InsertToLast(const T& newItem)
        {
            ListNode<T>* pCurrentIteration = this->m_pFirst;
            ListNode<T>* pNewItem = new ListNode<T>(NULL, newItem);
            POINTER_SANITY(pNewItem, "Failed allocating ListNode, no space available.");

            if (pCurrentIteration == NULL)
            {
                this->m_pFirst = pNewItem;
            }
            else
            {
                while (pCurrentIteration->GetNext() != NULL)
                {
                    pCurrentIteration = pCurrentIteration->GetNext();
                }

                pCurrentIteration->SetNext(pNewItem);
            }

            this->m_listSize++;
        }

        /**
        * Adds a new item to the beginning of the list.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void InsertToFirst(const T& newItem)
        {
            if (this->m_pFirst == NULL)
            {
                this->InsertToLast(newItem);
            }
            else
            {
                ListNode<T>* pNewItem = new ListNode<T>(this->m_pFirst, newItem);
                POINTER_SANITY(pNewItem, "Failed allocating ListNode, no space available.");
                this->m_pFirst = pNewItem;
                this->m_listSize++;
            }
        }

        /**
        * Deletes the last object on the list.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void DeleteLast()
        {
            if (this->m_pFirst != NULL)
            {
                ListNode<T>* pCurrentIteration = this->m_pFirst;
                ListNode<T>* pLastIteration = NULL;

                while (pCurrentIteration->GetNext() != NULL)
                {
                    pLastIteration = pCurrentIteration;
                    pCurrentIteration = pCurrentIteration->GetNext();
                }

                delete pCurrentIteration;
                pLastIteration = NULL;
                this->m_listSize--;
            }
        }

        /**
        * Deletes the first object on the list.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void DeleteFirst()
        {
            if (m_pFirst != NULL)
            {
                ListNode<T>* pSecond = this->m_pFirst->GetNext();
                delete this->m_pFirst;
                this->m_pFirst = pSecond;
                this->m_listSize--;
            }
        }

        /**
        * Returns the size of the list.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        size_t GetSize() const
        {
            return (this->m_listSize);
        }

        /**
        * Returns if the list is empty or not.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        bool IsEmpty() const
        {
            return (this->GetSize() == 0);
        }

        /**
        * Starting a list iteration.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void StartIteration()
        {
            m_pIterator = this->m_pFirst;
        }

        /**
        * Removes a certain object from the list of objects.
        *
        * @param removeObj The object to remove
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        void RemoveObject(const T& removeObj)
        {
            if (this->m_pFirst != NULL)
            {
                // Checking if it is the first.
                if (removeObj == this->m_pFirst->GetData())
                {
                    this->DeleteFirst();
                }
                else
                {
                    ListNode<T>* pCurrIterNode = this->m_pFirst;
                    ListNode<T>* pLastIterNode = NULL;

                    // Iterating through the clients...
                    while (pCurrIterNode != NULL)
                    {
                        const T& currObj = pCurrIterNode->GetData();

                        // If we have found a match.
                        if (currObj == removeObj)
                        {
                            // Remove the current client.
                            pLastIterNode->SetNext(pCurrIterNode->GetNext());
                            delete pCurrIterNode;
                            pCurrIterNode = NULL;
                            return;
                        }
                        else
                        {
                            pLastIterNode = pCurrIterNode;
                            pCurrIterNode = pCurrIterNode->GetNext();
                        }
                    }
                }
            }
        }

        /**
        * Updates an object's data.
        *
        * @param Obj The object to update.
        * @param newData The new data to change.
        *
        * @author Noam Rodrik
        * @since  10/03/2016
        */
        void UpdateObjectData(const T& Obj, const T& newData)
        {
            if (this->m_pFirst != NULL)
            {
                // Checking if it is the first.
                if (Obj == this->m_pFirst->GetData())
                {
                    this->m_pFirst->SetData(newData);
                }
                else
                {
                    ListNode<T>* pCurrIterNode = this->m_pFirst;
                    ListNode<T>* pLastIterNode = NULL;

                    // Iterating through the clients...
                    while (pCurrIterNode != NULL)
                    {
                        const T& currObj = pCurrIterNode->GetData();

                        // If we have found a match.
                        if (currObj == Obj)
                        {
                            // Remove the current client.
                            pCurrIterNode->SetData(newData);

                            return;
                        }
                        else
                        {
                            pLastIterNode = pCurrIterNode;
                            pCurrIterNode = pCurrIterNode->GetNext();
                        }
                    }
                }
            }
        }

        /**
        * Gets the current item in the iteration.
        *
        * @author Noam Rodrik
        * @since  09/30/2016
        */
        const T& GetCurrent(bool& o_bIsFinished, bool toIterate = true)
        {
            POINTER_SANITY(this->m_pIterator, "Invalid iterator given, an attempt to reverse engineer has occurred!");

            if (m_pIterator->GetNext() == NULL)
            {
                o_bIsFinished = true;
            }

            const T& currData = this->m_pIterator->GetData();

            if (toIterate)
            {
                this->m_pIterator = this->m_pIterator->GetNext();
            }

            return (currData);
        }

        /***************************************************************************************/
        /*                                      Data Members                                   */
        /***************************************************************************************/
        ListNode<T>* m_pFirst;
        size_t       m_listSize;
        ListNode<T>* m_pIterator;
        bool         m_toDelete;
    };
}

#endif