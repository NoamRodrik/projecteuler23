#include "AbundantVector.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main()
{
    time_t startTime = clock();
    Euler::AbundantVector abundantVector;
    time_t finishTime = clock();
    printf("It took %d milliseconds.\r\n", (finishTime - startTime));
    printf("Final result: %u\r\n", abundantVector.Result());
    system("pause");
    return 0;
}