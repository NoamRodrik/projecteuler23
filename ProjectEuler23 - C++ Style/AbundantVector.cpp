#include "AbundantVector.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

namespace Euler
{
    AbundantVector::AbundantVector() : m_lastSize(0), m_abundantQueue(NULL)
    {
        // Code Section
        for (uint16_t nIndex = MIN_ABUNDANT_NUMBER; nIndex <= MAX_ABUNDANT_LIMIT; ++nIndex)
        {
            // Creating a new abundant.
            Abundant abnNum = nIndex;

            // If the number is abundant.
            if (!(abnNum % 2 == 1 && abnNum % 5 == 1) &&
                (abnNum % MIN_DIV_ABUNDANT == 0 || abnNum.isAbundant()))
            {
                this->m_arrAbundants[this->m_lastSize++] = nIndex;
            }
        }

        this->AddSums();
    }

    void AbundantVector::AddSums()
    {
        // Code Section
        for (uint16_t nIndex = 0; nIndex < this->m_lastSize; ++nIndex)
        {
            // Getting the current abundant.
            const Abundant& currAbundant = this->m_arrAbundants[nIndex];

            // Inner iteration
            for (uint16_t nInnerIndex = 0; nInnerIndex < (this->m_lastSize / 2); ++nInnerIndex)
            {
                // Getting the current inner abundant.
                const Abundant& currInnerAbundant = this->m_arrAbundants[nInnerIndex];

                // Getting the summed abundant.
                if (currInnerAbundant + currAbundant < MAX_ABUNDANT_LIMIT)
                {
                    this->m_matchingAbundants[(currInnerAbundant + currAbundant)] = true;
                }
            }
        }
    }

    uint32_t AbundantVector::Result() const
    {
        // Variable Definition
        uint32_t nResult = 0;

        // Code Section
        for (uint16_t nIndex = 1; nIndex < MAX_ABUNDANT_LIMIT; ++nIndex)
        {
            nResult += this->m_matchingAbundants[nIndex] ? 0 : nIndex;
        }

        return (nResult);
    }
}