#ifndef _ABUNDANT_VECTOR__H_
#define _ABUNDANT_VECTOR__H_

#include "UniqueQueue.h"
#include "Abundant.h"

namespace Euler
{
    class AbundantVector
    {
        public:
            /**
             * Constructor
             */
            AbundantVector();

            /**
             * Virtual Destructor
             */
            virtual ~AbundantVector() {}

            /**
             * Adds sums.
             */
            void AddSums();

            /**
             * Returns the sum of all positive numbers
             * which cannot be written as the sum of two abundant numbers.
             */
            uint32_t Result() const;

        private:
            /**
             * Data Members
             */
            Abundant                m_arrAbundants[MAX_ABUNDANT_LIMIT];
            bool                    m_matchingAbundants[MAX_ABUNDANT_LIMIT];
            uint16_t                m_lastSize;
            Tools::Queue<Abundant>* m_abundantQueue;
    };
}

#endif  