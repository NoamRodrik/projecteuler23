#ifndef _UNIQUE_QUEUE__H_
#define _UNIQUE_QUEUE__H_

#include "List.h"

namespace Tools
{
    /**
     * A Queue class, made entirely of templates.
     */
    template <class T>
    class Queue
    {
        public:
            bool Contains(const T& object)
            {
                // Variable Definition
                bool bIterationIsFinished = false;

                // Getting the Iterator.
                for (this->m_queueList.StartIteration(); !bIterationIsFinished; )
                {
                    // Getting current object.
                    const T& currQueueObject = this->m_queueList.GetCurrent(bIterationIsFinished);

                    if (bIterationIsFinished)
                    {
                        return false;
                    }
                    else if (currQueueObject == object)
                    {
                        return true;
                    }
                }
            }

            bool AddObject(const T& object)
            {
                if (this->IsEmpty() || !this->Contains(object))
                {
                    this->m_queueList.InsertToLast(object);

                    return true;
                }

                return false;
            }

            uint32_t GetSize() const
            {
                return (this->m_queueList.GetSize());
            }

            void StartIteration()
            {
               this->m_queueList.StartIteration();
            }

            const T& GetCurrent(bool& o_bIsFinished, bool toIterate = true)
            {
                return (this->m_queueList.GetCurrent(o_bIsFinished, toIterate));
            }

            bool IsEmpty() const
            {
                return (this->m_queueList.IsEmpty());
            }

            ListNode<T>* SaveIterator() const
            {
                return (this->m_queueList.m_pIterator);
            }

            void SetIterator(ListNode<T>* pNewIterator)
            {
                SANITY(pNewIterator != NULL, "Iterator given was null.");
                this->m_queueList.m_pIterator = pNewIterator;
            }

        private:
            List<T> m_queueList;
    };
}

#endif