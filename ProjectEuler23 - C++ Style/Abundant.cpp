#include "Abundant.h"
#include <math.h>

namespace Euler
{
    Abundant::Abundant(uint16_t val): m_val(val) {}

    bool Abundant::isAbundant()
    {
        // Variable Definition
        uint16_t nCounter = 1;

        // Code Section
        for (uint16_t nIndex = 2; nIndex <= sqrt(this->m_val); ++nIndex)
        {
            if (this->m_val == nIndex*nIndex)
            {
                nCounter += nIndex;
            }
            else if (this->m_val % nIndex == 0)
            {
                nCounter += nIndex + static_cast<uint16_t>(this->m_val / nIndex);
            }
        }

        return (nCounter > this->m_val);
    }

    uint16_t Abundant::operator%(const Abundant& other) const
    {
        return (this->m_val % other.m_val);
    }

    uint16_t Abundant::operator+(const Abundant& other) const
    {
        return (this->m_val + other.m_val);
    }
}