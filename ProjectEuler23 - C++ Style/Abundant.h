#ifndef _ABUNDANT__H_
#define _ABUNDANT__H_

#include <stdint.h>

namespace Euler
{
    // Constant Global Definitions
    const uint16_t MAX_ABUNDANT_LIMIT  = 28123;
    const uint8_t  MIN_ABUNDANT_NUMBER = 12;
    const uint8_t  MIN_DIV_ABUNDANT    = 6;

    class Abundant
    {
        public:
            /**
             * Constructor
             */
            Abundant(uint16_t val = 0);

            /**
             * Virtual Destructor
             */
            virtual ~Abundant() {}

            /**
             * Returns if the number is abundant.
             */
            bool isAbundant();

            /**
             * Operator overloading.
             */
            uint16_t operator%(const Abundant& other) const;
            uint16_t operator+(const Abundant& other) const;

        private:
            /**
             * Data Members
             */
            uint16_t m_val;
    };
}

#endif